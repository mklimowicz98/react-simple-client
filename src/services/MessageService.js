import axios from 'axios'

const API_URL = "http://localhost:8080/messages/"

const getAllMessages = () => {
    return axios.get(API_URL + "getAll")
        .then((response) => {
            console.log(response.data)
        })
        .catch((error) => {
            console.log(error);
        });
}

const getMessage = (parameter) => {
    return axios.get(API_URL + "get", { params: { id: parameter } })
        .then((response) => {
            console.log(response.data)
        })
        .catch((error) => {
            console.log(error);
        });
}

const createMessage = (content, author) => {
    return axios.post(API_URL + "create", { content, author })
        .then((response) => {
            console.log(response.status)
        })
        .catch((error) => {
            console.log(error);
        });
}

const updateMessage = (id, content, author) => {
    return axios.put(API_URL + "update", { id, content, author })
        .then((response) => {
            console.log(response.status)
        })
        .catch((error) => {
            console.log(error);
        });
}

const deleteMessage = (parameter) => {
    return axios.delete(API_URL + "delete", { headers: { id: parameter } })
        .then((response) => {
            console.log(response.status)
        })
        .catch((error) => {
            console.log(error);
        });
}

const getMessagesStartingWith = (parameter) => {
    return axios.get(API_URL + "startingWith", { params: { prefix: parameter } })
        .then((response) => {
            console.log(response.data)
        })
        .catch((error) => {
            console.log(error);
        });
}

export default {
    getAllMessages,
    getMessage,
    createMessage,
    updateMessage,
    deleteMessage,
    getMessagesStartingWith
}
