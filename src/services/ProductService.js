import axios from 'axios'

const API_URL = "http://localhost:8080/product/"

const getAllProducts = () => {
    return axios.get(API_URL + "getAll")
        .then((response) => {
            console.log(response.data)
        })
        .catch((error) => {
            console.log(error);
        });
}

const getAllMatching = (name, maxCost, producer) => {
    return axios.post(API_URL + "getAllMatching", { name, maxCost, producer })
        .then((response) => {
            console.log(response.data)
        })
        .catch((error) => {
            console.log(error);
        });
}


export default {
    getAllProducts,
    getAllMatching
}
