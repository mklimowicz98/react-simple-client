import React, { useState } from "react";
import messageService from './services/MessageService.js'
import productService from './services/ProductService.js'

const App = () => {

  const [id, setId] = useState(1);
  const [content, setContent] = useState("content");
  const [author, setAuthor] = useState("author");
  const [name, setName] = useState("name");
  const [cost, setCost] = useState(1500);
  const [producer, setProducer] = useState("producer");

  //onChange
  const onChangeMessageId = (e) => {
    setId(e.target.value);
  };

  const onChangeContent = (e) => {
    setContent(e.target.value);
  };

  const onChangeAuthor = (e) => {
    setAuthor(e.target.value);
  };

  const onChangeName = (e) => {
    setName(e.target.value);
  };

  const onChangeCost = (e) => {
    setCost(e.target.value);
  };

  const onChangeProducer = (e) => {
    setProducer(e.target.value);
  };

  return (
    <div>

      <div>
        <label>id: </label>
        <input value={id} onChange={onChangeMessageId}></input>
      </div>

      <div>
        <label>content: </label>
        <input value={content} onChange={onChangeContent}></input>
      </div>

      <div>
        <label>author: </label>
        <input value={author} onChange={onChangeAuthor}></input>
      </div>


      <div>
        <button onClick={messageService.getAllMessages}> Get all messages </button>
      </div>

      <div>
        <button onClick={() => messageService.getMessage(id)}> Get message with id </button>
      </div>

      <div>
        <button onClick={() => messageService.createMessage(content, author)}> Create new message </button>
      </div>

      <div>
        <button onClick={() => messageService.updateMessage(id, content, author)}> Update message with id</button>
      </div>

      <div>
        <button onClick={() => messageService.deleteMessage(id)}> Delete message with id</button>
      </div>

      <div>
        <button onClick={() => messageService.getMessagesStartingWith(content)}>Get messages with prefix</button>
      </div>

      <div>
        <label>Zadanie 2 (Produkty)</label>
      </div>

      <div>
        <label>name: </label>
        <input value={name} onChange={onChangeName}></input>
      </div>

      <div>
        <label>max cost: </label>
        <input value={cost} onChange={onChangeCost}></input>
      </div>

      <div>
        <label>producer: </label>
        <input value={producer} onChange={onChangeProducer}></input>
      </div>

      <div>
        <button onClick={productService.getAllProducts}>Get all products</button>
      </div>

      <div>
        <button onClick={() => productService.getAllMatching(name, cost, producer)}>Get all matching</button>
      </div>

    </div>

  );
}

export default App;
